# Symfony 4 environnement avec VueJS

## Initialiser votre projet

### Etape 1 : 
* Installer NodeJS sur votre ordinateur : https://nodejs.org/fr/

### Etape 2 : 
* Installer Yarn sur votre ordinateur : https://yarnpkg.com/fr/docs/install#windows-stable

### Etape 3 :
#### Récupérer l'environnement pour un nouveau projet
* Rendez-vous sur cette page : https://gitlab.com/flixflex/Environnement/Symfony-4-VueJS
* Cliquez sur le bouton ``download`` en fichier zip
* Extraire l'archive ... Et voilà, l'environnement est prêt !

### Etape 4 : 
* Faites un composer install à la racine de votre projet : ``php path/to/your/composer.phar install``

### Etape 5 : 
* Faites un yarn install à la racine de votre projet : ``yarn install``

Après ça le projet est prêt ! Mais n'oubliez pas ...

## Après chaque modification de vos fichiers Vue ou de la configuration

### Compilez les assets : 
* manuellement : ``yarn encore dev``
* automatiquement à chaque changement sur les fichiers : ``yarn encore dev --watch``
* pour la prod : ``yarn encore production``
* doc : https://symfony.com/doc/current/frontend/encore/simple-example.html

## Ajouter un plugin Vue JS

Certaines personnes ont déjà crée des composants VueJs qui peuvent déjà faire ce que vous souhaitez faire, dans ce cas il est plus intéressant de les installer et de les utiliser.

Prenons en exemple un composant de table :
* https://github.com/spatie/vue-table-component
* Faites la commande yarn donné sur cette page : ``yarn add vue-table-component``
* Sur le fichier .vue sur lequel vous souhaitez utiliser ce composant faites un import : ``import { TableComponent, TableColumn } from 'vue-table-component';`` dans la balise script

Et voilà, vous pouvez maintenant utiliser ce composant dans votre fichier vue js