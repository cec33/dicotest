<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class DicoController extends AbstractController 
{
    public function index()
    {
        return $this->render("dico/index.html.twig");
    }
}
