<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class TestController extends AbstractController
{
    /** @var Array $datas */
    private $datas = ['Pomme', 'Poire', 'Citron', 'Orange'];

    public function index()
    {
        return $this->render('test/index.html.twig', [
            'controller_name' => 'TestController',
            'datas'           => $this->datas
        ]);
    }

    /**
     * Change the datas in the $datas array
     * @param Request $request
     * @return array
     */
    public function changeDatas(Request $request)
    {
        if(empty($request->request->get('datas'))) {
            throw new \Exception('Vous n\'avez aucun fruit favoris');
        }
        $this->datas = explode(',', $request->request->get('datas'));
        return new JsonResponse($this->datas);
    }
}
